var board = document.querySelector("#board"), boardstring;
var found = document.querySelector(".found"), foundWords = [];
var highscore = document.querySelector(".highscore"), highscores = [];
var total = 0, word=[];

$(document).ready(function(){
	
	$.getJSON("http://www.diamundo.nl:3000/newboard", function(data, status){
		boardstring = JSON.parse(JSON.stringify(data)).board;
		
		for (i = 0; i < boardstring.length; i++) {
		      board.innerHTML += ('<li data-order=' + i + ' data-val=' + boardstring.charAt(i) + '><span>' + boardstring.charAt(i) + '</span></li>'); 
		}
	});

	$.getJSON("http://www.diamundo.nl:3000/highscore", function(data, status){
		highscores = JSON.parse(JSON.stringify(data)).highscores;
		
		highscore.innerHTML = "";
		for (i = 0; i < highscores.length; i++) {
		      highscore.innerHTML += ('<li data-order=' + i + '><span>' + highscores[i].Name + ": " + highscores[i].Score + ' pts</span></li>'); 
		}
		highscore.innerHTML += "<li data-order="+ i +"><span>&nbsp;</span></li>"; //extra newline before foundWords
	});

	
	function wordToString(){
		var text = "";
		for(var i=0; i<word.length; i++){
			text += $("li:eq("+ word[i]+")").attr('data-val');
		}
		return text;
	}
	
	function setText() {
		$("#entered").val( wordToString() );
	}
	
	function updateFoundWords(){
		found.innerHTML = "";
		for(var i=0; i<foundWords.length; i++){
			found.innerHTML += "<li data-order=" + i + "><span>" + foundWords[i] + "</span></li>";
		}
	}
	
	function testWord() {
		if( foundWords.indexOf(wordToString()) == -1 && word.length > 0) {
		
			$.getJSON("http://www.diamundo.nl:3000/checkword?word=" + wordToString().toLowerCase(), function(data, status){
				var res = JSON.parse(JSON.stringify(data));
				console.log("Testing: '" + wordToString() + "' => res\n" + JSON.stringify(res));
				if(res.isValid){
					foundWords.push(wordToString());
					updateFoundWords();
					word = [];
					total += res.score;
					$(".total").text(total + " pts");
					$("li").removeClass("disabled");
					$("li").removeClass("selected");
				} else {
					$(".errors").text("The word '"+ wordToString() +"' is not a valid word!");
				}
			});
		} else if( foundWords.indexOf(wordToString()) >= 0 ){
			$(".errors").text("The word '"+ wordToString() +"' was already found!");
		}
	}
	
	$("ul").on('click', 'li', function () {
		var index = $("li").index( this );
	
		if( ( ! $("li:eq("+ index +")").hasClass("disabled") ) || ( $("li:eq("+ index +")").hasClass("disabled") && $("li:eq("+ index +")").hasClass("selected") && word[ word.length - 1] == index ) ) {
			$(".errors").text(" "); //clear errors
			
			if( $("li:eq("+ index +")").hasClass("selected") ) {
				word.pop();
				setText();
				$("li:eq("+ index +")").removeClass("selected");
			} else {
				word.push(index);
				setText();
			}
		
			if( word.length > 0) {
				index = word[ word.length - 1 ];
				$("li:eq("+ index +")").addClass("selected");
		
				$("li").addClass("disabled");
				var enable = [];
				switch(index){
					case 0:  enable.push(1, 4, 5); break;
					case 1:  enable.push(0, 2, 4, 5, 6); break;
					case 2:  enable.push(1, 3, 5, 6, 7); break;
					case 3:  enable.push(2, 6, 7); break;
					case 4:  enable.push(0, 1, 5, 8, 9); break;
					case 5:  enable.push(0, 1, 2, 4, 6, 8, 9, 10); break;
					case 6:  enable.push(1, 2, 3, 5, 7, 9, 10, 11); break;
					case 7:  enable.push(2, 3, 6, 10, 11); break;
					case 8:  enable.push(4, 5, 9, 12, 13); break;
					case 9:  enable.push(4, 5, 6, 8, 10, 12, 13, 14); break;
					case 10: enable.push(5, 6, 7, 9, 11, 13, 14, 15); break;
					case 11: enable.push(6, 7, 10, 14, 15); break;
					case 12: enable.push(8, 9, 13); break;
					case 13: enable.push(8, 9, 10, 12, 14); break;
					case 14: enable.push(9, 10, 11, 13, 15); break;
					case 15: enable.push(10, 11, 14); break;
				}
				
				for(var i=0; i < enable.length; i++){
					if( word.indexOf( enable[i]) == -1 ) {
						$("li:eq("+ enable[i] +")").removeClass("disabled");
					} //only enable items that aren't already selected
				}
				
			} else { //no letters selected anymore
				$("li").removeClass("disabled");
				$("li").removeClass("selected");
			}
		} else {
			$(".errors").text("The letter '"+ $("li:eq("+ index +")").attr("data-val") + "' cannot be selected!");
	
		}
	});
	
	$("#entered").on("input", function() {
		setText();
	});
	
	$("#test").on('click', function () {
		testWord();
	});
	
	$(document).on('keypress', function(e) {
		if(e.which == 13) {
			testWord();
	    }
	});

	$("#save").on('click', function () {
		if( total > 0) {
			var askName = prompt("Please enter your name:","");
			
			if(askName != null && askName != "") { //user has not cancelled
				$.post("http://www.diamundo.nl:3000/highscore", {
				    Name : askName,
				    Score : total,
				    DateTime : ( new Date().getFullYear() + "-" + (new Date().getMonth()+1)  + "-" + new Date().getDate() + " " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds() )
				}, function(data, status){ /* do nothing on response */ });
				
				if( confirm("Start a new game?") ) {
					window.location.reload();
				}
			}
		}
	});
	
});
